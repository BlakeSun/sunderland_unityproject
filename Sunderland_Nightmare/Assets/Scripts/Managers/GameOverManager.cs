﻿using UnityEngine;

public class GameOverManager : MonoBehaviour
{
    public PlayerHealth playerHealth;


    Animator anim;


    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        if (playerHealth.currentHealth <= 0)
        {
            anim.SetTrigger("GameOver");
        }

        if(Ammo.AmmoClip <= 7 && Ammo.Reloading == false)
        {

            anim.SetTrigger("ReloadPlease");
            anim.ResetTrigger("DoneReloading");

        }

        if(Ammo.AmmoClip > 7 && Ammo.Reloading == false)
        {
            anim.ResetTrigger("ReloadPlease");
            
            anim.SetTrigger("DoneReloading");

        }

        if(Ammo.Reloading == true)
        {

            anim.SetTrigger("Reloading");
            Invoke("Reloading", 2);
            

        }

        

    }
    private void Reloading()
    {

        //print("hi");
        anim.ResetTrigger("Reloading");
        anim.SetTrigger("DoneReloading");
        Ammo.Reloading = false;

    }
}

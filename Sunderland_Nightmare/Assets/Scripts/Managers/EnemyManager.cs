﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public GameObject enemy;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;
    public static int zombiecounttotal = 0;
    public static int level = 1;
    public static bool zombiespawn = true;



    public void Start ()
    {
        
        InvokeRepeating ("Spawn", spawnTime, spawnTime);
        
    }

    void FixedUpdate()
    {

        //Invoke("Check", 5);
        if (Round.start == true)
        {

            Start();
            Round.start = false;

        }
    }


    void Spawn ()
    {

        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }

        if(zombiecounttotal >= (5*level))
        {

            CancelInvoke("Spawn");
            zombiespawn = false;

            if(tag == "Boss")
            {

                
                Instantiate(enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);

            }

            return;

        }

        

        Instantiate (enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);



        zombiecounttotal += 1;
        print("Total: " + zombiecounttotal);


    }

   
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class Ammo : MonoBehaviour
{

    public static int AmmoClip;
    public static int AmmoReserve;
    public static bool Reloading;
    public static int AmmoClipShot;
    public static int AmmoReserveShot;



    Animator anim;


    Text text;


    void Awake()
    {
        text = GetComponent<Text>();
        AmmoClip = 30;
        AmmoReserve = 150;
        Reloading = false;

        AmmoClipShot = 2;
        AmmoReserveShot = 50;



    }


    void Update()
    {

        if (PlayerShooting.shotgun == false)
        {

            text.text = AmmoClip + "/30     " + AmmoReserve;

        }
        
        if (PlayerShooting.shotgun == true)
        {

            text.text = AmmoClipShot + "/2     " + AmmoReserveShot;

        }
        

    }
}

//void Update()
//    {

//        //GameObject[] enemyArray = GameObject.FindGameObjectsWithTag("Zombie");
//        //print("Number of Enemies" + enemyArray.Length);

//    }

//}
    

       

    

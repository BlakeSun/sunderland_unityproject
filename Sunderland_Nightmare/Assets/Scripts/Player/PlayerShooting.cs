﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float switchtimertime = .15f;
    public float range = 100f;
    public ParticleSystem smoke;
    public ParticleSystem bulletcasing;

    float switchtimer;
    float timer;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;
    public static bool shotgun = false;
    public Image imageshot;
    public Image imagear;


    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();

    }


    void Update ()
    {
        timer += Time.deltaTime;
        switchtimer += Time.deltaTime;

        if (shotgun == true)
        {

            damagePerShot = 50;
            timeBetweenBullets = 0.5f;


        }

        if (shotgun == false)
        {

            damagePerShot = 20;
            timeBetweenBullets = .15f;

        }

        if (Input.GetButton("Switch") && switchtimer >= switchtimertime && Time.timeScale != 0)
        {

            //print("Switch");
            //print(shotgun);

            if (shotgun == false)
            {
                //print("Switch False");
                shotgun = true;
                imagear.enabled = true;
                imageshot.enabled = false;
                switchtimer = 0;

            }

            else if (shotgun == true)
            {
                //print("Switch True");
                shotgun = false;
                imagear.enabled = false;
                imageshot.enabled = true;
                switchtimer = 0;

            }

        }


        if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {

            if (Ammo.AmmoClip == 0f || Ammo.AmmoClipShot == 0f)
            {

                return;

            }

            
            else
            {
                Shoot();
                if (shotgun == false)
                {

                    Ammo.AmmoClip -= 1;

                }

                if (shotgun == true)
                {

                    Ammo.AmmoClipShot -= 1;

                }
                
                smoke.Stop();
                bulletcasing.Play();
            }
        }

        if(timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects ();
        }

        if (Input.GetButton("Reload"))
        {

            
            Invoke("Reloading", 2);
            smoke.Play();
            Ammo.Reloading = true;
            


        }
        
    }

    


    void Reloading()
    {
        if (shotgun == false)
        {

            Ammo.AmmoReserve -= 30 - Ammo.AmmoClip;
            Ammo.AmmoClip = 30;
            if (Ammo.AmmoReserve < 0)
            {

                Ammo.AmmoClip += Ammo.AmmoReserve;
                Ammo.AmmoReserve = 0;
                smoke.Stop();



            }
            if (Ammo.AmmoReserve == 0)
            {

                return;

            }

        }

        if (shotgun == true)
        {

            Ammo.AmmoReserveShot -= 2 - Ammo.AmmoClipShot;
            Ammo.AmmoClipShot = 2;
            if (Ammo.AmmoReserve < 0)
            {

                Ammo.AmmoClipShot += Ammo.AmmoReserveShot;
                Ammo.AmmoReserveShot = 0;
                smoke.Stop();

            }
            if (Ammo.AmmoReserveShot == 0)
            {

                return;

            }



        }
        

    }

    

    public void DisableEffects ()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    void Shoot ()
    {
        timer = 0f;

        gunAudio.Play ();

        gunLight.enabled = true;

        gunParticles.Stop ();
        gunParticles.Play ();

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
        {
            
             
                
                

            
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
            if(enemyHealth != null)
            {
                enemyHealth.TakeDamage (damagePerShot, shootHit.point);
            }
            gunLine.SetPosition (1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
    }
}

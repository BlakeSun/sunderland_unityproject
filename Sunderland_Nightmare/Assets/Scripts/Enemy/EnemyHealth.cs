﻿using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public float sinkSpeed = 2.5f;
    public int scoreValue = 10;
    public AudioClip deathClip;
    public GameObject Ammo;
    public GameObject Health;
    public Transform[] spawnPoints;
    public static int zombiecount = 0;



    Animator anim;
    AudioSource enemyAudio;
    ParticleSystem hitParticles;
    CapsuleCollider capsuleCollider;
    bool isDead;
    bool isSinking;


    void Awake ()
    {
        anim = GetComponent <Animator> ();
        enemyAudio = GetComponent <AudioSource> ();
        hitParticles = GetComponentInChildren <ParticleSystem> ();
        capsuleCollider = GetComponent <CapsuleCollider> ();

        currentHealth = startingHealth;
    }


    void Update ()
    {
        if(isSinking)
        {
            transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
        }
    }


    public void TakeDamage (int amount, Vector3 hitPoint)
    {
        if(isDead)
            return;

        enemyAudio.Play ();

        currentHealth -= amount;
            
        hitParticles.transform.position = hitPoint;
        hitParticles.Play();

        if(currentHealth <= 0)
        {
            Death ();
        }
    }


    void Death ()
    {
        isDead = true;

        capsuleCollider.isTrigger = true;

        anim.SetTrigger ("Dead");

        enemyAudio.clip = deathClip;
        enemyAudio.Play ();
        ScoreManager.score += scoreValue;
        Destroy(gameObject, 2f);
        zombiecount += 1;
        print(zombiecount);

        if (tag == "Boss")
        {

            ScoreManager.score += 90;
            Invoke("SpawnHealth", 1);
        }
        EnemyManager.zombiecounttotal -= 1;
        
        if (zombiecount == 15)
        {
            print("Spawn Ammo");
            Invoke("SpawnAmmo", 1);
            zombiecount = 0;

        }

            
    }

    void SpawnAmmo()
    {
        print("SpawnAmmo");
        int spawnPointIndex = 0;

        Instantiate(Ammo, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);

    }

    void SpawnHealth()
    {

        print("SpawnHealth");

        int spawnPointIndex = 0;
        Instantiate(Health, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);

    }

    public void StartSinking ()
    {
        GetComponent <UnityEngine.AI.NavMeshAgent> ().enabled = false;
        GetComponent <Rigidbody> ().isKinematic = true;
        isSinking = true;
        ScoreManager.score += scoreValue;
        Destroy (gameObject, 2f);
    }
}

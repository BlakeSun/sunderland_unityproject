﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Round : MonoBehaviour

    

{

    public static bool start = false;

    Text text;

    // Start is called before the first frame update
    void Awake()
    {

        text = GetComponent<Text>();

    }

    void Start()
    {

        InvokeRepeating("Check", 4, 10f);

    }

    // Update is called once per frame
    void Update()
    {

        text.text = "Round: " + EnemyManager.level;

    }

    void Check()
    {

        if (EnemyManager.zombiecounttotal == 0 && EnemyManager.zombiespawn == false)
        {

            EnemyManager.zombiespawn = true;

            EnemyManager.level += 1;

            print("level: " + EnemyManager.level);

            start = true;


        }


    }

}
